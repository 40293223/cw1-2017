﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace conference_system
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        int ConferencePrice;

        public Invoice(string Name,string Institution ,string ConferenceName)
        {
            InitializeComponent();
            LblAttendeeName.Content = Name;
            LblAtndeInstName.Content = Institution;
            LblAtendeConfName.Content = ConferenceName;          
        }

        public void getCost(string RegistrationType,string PresentrType,string PaidType)
        {
            try
            {
                if (RegistrationType == "Full")
                {
                    ConferencePrice = 500;
                    if(PresentrType == "True")
                    {
                        ConferencePrice -= Convert.ToInt32(500*0.10);  
                    }
                }
                else if (RegistrationType == "Student")
                {
                    ConferencePrice = 300;
                    
                    if (PresentrType == "True")
                    {
                        ConferencePrice -= Convert.ToInt32(300 * 0.10);
                    }
                }
                else if (RegistrationType == "Organiser")
                {
                    ConferencePrice = 0;
                }

                if (PaidType == "False")
                {
                    LblAtendePaid.Content = "£" + ConferencePrice + " is not paid ";
                }
                else
                {
                    LblAtendePaid.Content = "£" + ConferencePrice;
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
    }
}
