﻿// Author Name:
// Usman Ghani
// Class Description:
// This is an Institute Class which is associated with the Attendee Class and is used to
// set and get the values of Attendee's Institute who is attending the conference. 
// The data type for each property are characterised according to the textbox
// values which will use in the GUI form.
// Date last modified:
// 15/10/2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conference_system
{
    public class Institution
    {
        private string Address;
        private string InstitutionName;

        public string address
        {
            get
            {
                return Address;
            }
            set
            {
                Address = value;
            }
        }
            public string institutionName
            {
                get
                {
                    return InstitutionName;
                   
                }
                set
                {
                    InstitutionName = value;
                }
            }
        }
    }
