﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace conference_system
{
    /// <summary>
    /// Interaction logic for Certificate.xaml
    /// </summary>
    public partial class Certificate : Window
    {
        public Certificate(string firstName,string secondName,string confName,string paperTitle)
            
        {
            InitializeComponent();
            LblCertFirstName.Content = firstName;
            LblCertScndName.Content = secondName;
            LblConfName.Content = confName;
            LblCertPaprTitle.Content = paperTitle;

        }

        private void BtnCertClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
