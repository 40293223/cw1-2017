﻿// Author Name: Usman Ghani
// Description of class purpose: This is a Attendee Class for the Attendee data
// Date last modified: 14/10/2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conference_system
{
    //inheritance between Attendee class and Person class
    public class Attendee : Person
    {
        private int AttendeeRef;
        private string ConferenceName;
        private bool Paid;
        private string PaperTitle;
        private bool Presenter;
        private string RegistrationType;
        private string InstitutionName;
        private string InstitutionAddress;

        public int attendeeRef
        {
            get
            {
                return AttendeeRef;
            }

            set
            {
                //condition for attendee reference value
                if(value < 40000 || value > 60000)
                {
                    //throw error message if value will be incorrect
                    throw new ArgumentException("Please Enter a Correct Value." + Environment.NewLine + "Example: 40000 TO 60000");

                }

                AttendeeRef = value;
            }
        }

        public string conferenceName
        {
            get
            {
                return ConferenceName;
            }
            set
            {
                //condition for conference field
                if (string.IsNullOrEmpty(value))
                {
                    //throw error message for entering conference name
                    throw new ArgumentException("Please Enter a value Conference Name" + Environment.NewLine + "Like: Computing 2017");
                }
                ConferenceName = value;
            }
        }

        public bool paid
        {
            get
            {
                return Paid;
            }

            set
            {
                Paid = value;
            }
        }

        public string paperTitle
        {
            get
            {
                return PaperTitle;
            }

            set
            {
                //condition for presenter
                if (Presenter.ToString() == "True")
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        //message throw when set command run without entering Paper Title and paper title field is active
                        throw new AggregateException("Please enter a value PaperTitle");
                    }
                }

                PaperTitle = value;
            }
        }

        public bool presenter
        {
            get
            {
                return Presenter;
            }
            set
            {
                Presenter = value;
            }
        }

        public string registrationType
        {
            get
            {
                return RegistrationType;
            }
            set
            {
                RegistrationType = value;
            }
        }

        public string institutionName
        {
            get
            {
                return InstitutionName;
            }
            set
            {
                InstitutionName = value;
            }
        }

        public string institutionAddress
        {
            get
            {
                return InstitutionAddress;
            }
            set
            {
                InstitutionAddress = value;
            }
        }
    }
}
