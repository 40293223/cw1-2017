﻿// Author Name: Usman Ghani
// Description of class purpose: This is a Person Class for Person's First and Last Name
// Date last modified: 14/10/2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conference_system
{
    public class Person
    {
        private string FirstName;
        private string SecondName;


        public string firstName
        {
            get
            {
                return FirstName;
            }
            set
            {
                //condition for First Name
                if (string.IsNullOrEmpty(value))
                {
                    //message throw if First Name field remain empty and user hit set button
                    throw new ArgumentException("Please Enter a value First Name" + Environment.NewLine + "Example: Susan");
                }

                FirstName = value;
            }
        }

        public string secondName
        {
            get
            {
                return SecondName;
            }
            set
            {
                //condition for Second Name
               if (string.IsNullOrEmpty(value))
                {
                    //message throw if Second Name field remain empty and user hit set button
                    throw new ArgumentException("Please Enter a value Second Name" + Environment.NewLine + "Example: Williamson");
                }
                SecondName = value;
            }
        }
    }
}
