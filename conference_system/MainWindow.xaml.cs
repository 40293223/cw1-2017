﻿// Author Name: Usman Ghani
// Description of purpose: This is a GUI form which will use to enter the Attendee data
// and also have access, in case to get the certificate and invoice.
// Date last modified: 14/10/2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace conference_system
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Attendee ClassAttendee = new Attendee();

        public MainWindow()
        {
            InitializeComponent();

            TxtBoxPaperTitle.IsEnabled = false;

            CmbRegType.SelectedIndex = 0;

            CmbPaid.SelectedIndex = 0;

            CmbPresenter.SelectedIndex = 0;
        }

        private void ButtonSet_Click(object sender, RoutedEventArgs e)
        {        
            if(SetDetails() == true)
            {
                //Method Set Details
                MessageBox.Show("Your value has been set");
            }
              
        }

        bool SetDetails()
        {
            /* try, catch used to handle errors.
           Code in a try block identifies a block of code followed by one catch block.*/

            try
            {
                //Set Properties from Class Person
                ClassAttendee.firstName = TxtBoxFirstName.Text;

                ClassAttendee.secondName = TxtBoxSecondName.Text;
               
                //Set Properties from Class Attendee

                if(string.IsNullOrEmpty(TxtBoxAttRef.Text))
                {
                    MessageBox.Show("Please Enter a Attendee Ref Id Value");
                    return false;
                }
                else
                {
                    ClassAttendee.attendeeRef = Convert.ToInt32(TxtBoxAttRef.Text);
                }
                
                ClassAttendee.conferenceName = TxtBoxConfName.Text;

                if (CmbRegType.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select a Registration Type");
                    return false;
                }
                else
                {
                    ComboBoxItem RegItem = (ComboBoxItem)CmbRegType.SelectedItem;
                    ClassAttendee.registrationType = RegItem.Content.ToString();
                }

                if (CmbPaid.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select a Paid Type");
                    return false;
                }
                else
                {
                    ComboBoxItem paidItem = (ComboBoxItem)CmbPaid.SelectedItem;
                    ClassAttendee.paid = Convert.ToBoolean(paidItem.Content.ToString());
                }

                if(CmbPresenter.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select a Presenter Type");
                    return false;
                }
                else
                {
                    ComboBoxItem presenterItem = (ComboBoxItem)CmbPresenter.SelectedItem;
                    ClassAttendee.presenter = Convert.ToBoolean(presenterItem.Content.ToString());
                }


                ClassAttendee.paperTitle = TxtBoxPaperTitle.Text;

                ClassAttendee.institutionName = TxtBoxInstName.Text;

                ClassAttendee.institutionAddress = TxtBoxInstAddress.Text;
            }

            catch (Exception error)
            {
               MessageBox.Show(error.Message);
               return false;
            }

            return true;
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            //Clear all text boxes values
            try
            {
                TxtBoxFirstName.Clear();
                TxtBoxSecondName.Clear();
                TxtBoxAttRef.Clear();
                TxtBoxConfName.Clear();
                TxtBoxInstName.Clear();
                TxtBoxPaperTitle.Clear();
                TxtBoxInstAddress.Clear();
                CmbPaid.SelectedIndex = 0;
                CmbPresenter.SelectedIndex = 0;
                CmbRegType.SelectedIndex = 0;
            }
            catch (Exception error)
            {

                error.ToString();
            }

        }

        private void CmbRegType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CmbPresenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // code in a try block identifies a block of code followed by one catch block. 
            try
            {

                /* The following code will activate the text box of paper title, if the user selects
                'True' from 'Presenter' and it became deactivate if the the user selects 'False'.*/

                if (((ComboBoxItem)CmbPresenter.SelectedItem).Content.ToString() == "True")
                {
                    TxtBoxPaperTitle.IsEnabled = true;
                }
                else if (((ComboBoxItem)CmbPresenter.SelectedItem).Content.ToString() == "False")
                {
                    TxtBoxPaperTitle.IsEnabled = false;
                    TxtBoxPaperTitle.Text = string.Empty;
                }

            }
            catch (Exception error)
            {
                error.ToString();
            }
        }

        private void ButtonCerti_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool returnVal = true;
                Certificate certificate = new Certificate(ClassAttendee.firstName, ClassAttendee.secondName, ClassAttendee.conferenceName, ClassAttendee.paperTitle);

                if (((ComboBoxItem)CmbPresenter.SelectedItem).Content.ToString() == "False")
                {
                    certificate.LblCertPresPaper.Visibility = Visibility.Hidden;
                    if (!string.IsNullOrEmpty(ClassAttendee.paperTitle))
                    {
                        MessageBox.Show("Please Set all Values");
                        returnVal = false;
                    }
                    
                }

                if (ClassAttendee.firstName == null || ClassAttendee.secondName == null || ClassAttendee.conferenceName == null)
                {
                    MessageBox.Show("Please Set all values");
                }
                else
                {
                    if (CmbPresenter.Text == "True" && string.IsNullOrEmpty(ClassAttendee.paperTitle))
                    {
                        MessageBox.Show("Please Set all values");
                    }
                    else
                    {
                        if (returnVal == true)
                        {
                            certificate.Show();
                        }
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }

        private void ButtonGet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Method Get Details

                if (GetDetails() == true)
                {
                    MessageBox.Show("Your values has been get");
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
           
        }

        bool GetDetails()
        {
            try
            {

                //Get Properties From Class Person
                TxtBoxFirstName.Text = ClassAttendee.firstName;
                TxtBoxSecondName.Text = ClassAttendee.secondName;

                //Get Properties From Class Attendee
                TxtBoxAttRef.Text = Convert.ToInt32(ClassAttendee.attendeeRef).ToString();
                TxtBoxConfName.Text = ClassAttendee.conferenceName;
                CmbPaid.Text = Convert.ToBoolean(ClassAttendee.paid).ToString();
                TxtBoxPaperTitle.Text = ClassAttendee.paperTitle;
                CmbPresenter.Text = Convert.ToBoolean(ClassAttendee.presenter).ToString();
                CmbRegType.Text = ClassAttendee.registrationType;
                TxtBoxInstName.Text = ClassAttendee.institutionName;
                TxtBoxInstAddress.Text = ClassAttendee.institutionAddress;

            }
            catch (Exception ErrorHandling)
            {
                MessageBox.Show(ErrorHandling.Message);
                return false;
            }

            return true;
        }

        private void ButtonInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Invoice frmInvoice = new Invoice(ClassAttendee.firstName + " " + ClassAttendee.secondName,ClassAttendee.institutionName,ClassAttendee.conferenceName);
                frmInvoice.getCost(CmbRegType.Text,CmbPresenter.Text,CmbPaid.Text);

                if (ClassAttendee.firstName == null || ClassAttendee.secondName == null || ClassAttendee.conferenceName == null)
                {
                    MessageBox.Show("Please Set all values");
                }
                else
                {
                    frmInvoice.Show();
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
    }
}
